from PyQt5 import QtWidgets, uic
import sys


def run_app():
    app = QtWidgets.QApplication([])
    win = uic.loadUi("../client/interface.ui")  # расположение вашего файла .ui

    win.show()
    sys.exit(app.exec())
